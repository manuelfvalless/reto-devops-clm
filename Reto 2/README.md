# Reto 2

El reto consiste en ejecutar docker-compose, para ello se requiere lo siguiente:


Instalar docker-compose
-----------------------

Se recomienda seguir los pasos de instalación de la siguiente pág: https://docs.docker.com/compose/install/

Docker compose
--------------

En el directorio donde se encuentra el archivo docker-compose.yml, ejecutar comando

- docker-compose up -d

Esto crear la imagen de nginx y los contenedores de la app y nginx respectivamente, una vez creado lo anterior, abrir navegador y en la barra de direcciones, escribir localhost:8081. Se muestra que es un sitio no seguro, hacer clic en enlace de continuar, posterior a ello, se solicitará usuario y contraseña, para ambos casos indicar: clm

Para detener contenedor, ejecutar comando:

- docker-compose stop

NOTA: dentro de la carpeta Reto 2, hay un sub directorio denominado app, la misma contiene el fuente de la app, para construir la imagen es requerido una carpeta con nombre "node_modules", esta se genera ejecutando el comando npm install, en caso de que no se encuentre la carpeta indicada, ejecutar el comando mencionado en el directorio del fuente.
