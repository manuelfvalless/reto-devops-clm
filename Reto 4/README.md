# Reto 4

El reto consiste en realizar deploy de la app node en un cluster de kubernetes, para ello se requiere:


Instalar kubectl y minikube
---------------------------

Para instalar kubectl, seguir los pasos indicados de la pág: https://v1-18.docs.kubernetes.io/docs/tasks/tools/install-kubectl/

Para instalar minikube, seguir los pasos indicados de la pág: https://v1-18.docs.kubernetes.io/es/docs/tasks/tools/install-minikube/


Crear namespace
---------------

Finalizada la instalación, ejecutar los siguientes comando:

- Para iniciar minikube, ejecutar comando:
    - minikube start
- Cargar imagenes que se encuentran local al cluster de minikube, ejecutar comandos:
    - eval $(minikube docker-env)
    - minikube cache add nombre-imagen-app-node (utilizar la que fue creada en el reto 1)
    - minikube cache reload
    - minikube cache list
- Crear namespace, ejecutar comando:
    - kubectl create namespace clm


Deploy
------

Ejecutar los siguientes comandos para realizar deploy (estar dentro del directorio que contiene los archivos deployment.yaml, service.yaml, hpa.yaml):

- kubectl apply -f deployment.yaml -n clm
- kubectl apply -f service.yaml -n clm
- kubectl apply -f hpa.yaml -n clm
- Para ver los pods creados, ejecutar comando:
    - kubectl get pods -n clm
- Puede verificar sus servicios dentro del cluster ejecutando el siguiente comando:
    - kubectl get service -n clm
- Ejecute el siguiente comando para habilitar External-IP para su aplicación:
    - minikube service <service_name>
- El comando anterior abrirá automáticamente su aplicación Node.js en el navegador.
