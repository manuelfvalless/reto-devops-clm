# Reto 1

El reto consiste en crear una imagen docker con el fuente que se encuentra en la carpeta, para ello es requerido realizar los siguientes pasos:


Instalar node y npm en la máquina en la cual se va a construir la imagen
------------------------------------------------------------------------

Dependiendo del so de la máquina, la instalación se puede realizar a través del gestor de paquetes o bien por línea de comandos:

- Distribuciones linux basadas en debian y ubuntu:
    - sudo apt update
    - sudo apt install nodejs
    - sudo apt install npm
    - Verificar instalación:
        - node --version
        - npm --version

- Distribuciones linux basadas en rpm
    - curl -sL https://rpm.nodesource.com/setup_12.x | sudo bash -
    - sudo yum install nodejs
    - Verificar instalación:
        - node --version
        - npm --version
    - Para poder construir módulos nativos desde npm, necesitaremos instalar las herramientas de desarrollo y las bibliotecas:
        - sudo yum install gcc-c++ make


Instalar docker
---------------

Se recomienda seguir los pasos de instalación de la siguiente pág: https://docs.docker.com/get-docker/

Finanlizado lo anterior, para ejecutar el comando docker sin sudo (en caso de linux), agregue su nombre de usuario al grupo docker:

- sudo usermod -aG docker ${USER}

Para aplicar la nueva membresía de grupo, cierre la sesión del servidor y vuelva a iniciarla o escriba lo siguiente:

- su - ${USER}

Confirme que ahora su usuario se agregó al grupo docker escribiendo lo siguiente:

- id -nG

Si debe agregar al grupo docker un usuario con el que no inició sesión, declare dicho nombre de usuario de forma explícita usando lo siguiente:

- sudo usermod -aG docker username


Contruir la imagen
------------------

Terminada las instalaciones correspondientes, se ejecutan los siguientes comandos (estar dentro del directorio donde se encuentra el funete del proyecto):

- npm install
- docker build -t node_app_clm -f Dockerfile .
- Para ejecutar contenedor:
    - docker run --name node-app node_app_clm
- Para detener contenedor:
    - cerrar consola y abrir nuevamente.
    - ejecutar docker stop
